package belog.dao;

import belog.dao.common.CommonDao;
import belog.pojo.po.Links;

/**
 * @author Beldon
 */
public interface LinksDao extends CommonDao<Links> {
}
