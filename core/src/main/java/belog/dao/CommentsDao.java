package belog.dao;

import belog.dao.common.CommonDao;
import belog.pojo.po.Comments;

/**
 * @author Beldon
 */
public interface CommentsDao extends CommonDao<Comments> {
}
