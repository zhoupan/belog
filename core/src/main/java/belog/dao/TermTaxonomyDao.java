package belog.dao;


import belog.dao.common.CommonDao;
import belog.pojo.po.TermTaxonomy;

/**
 * @author Beldon
 */
public interface TermTaxonomyDao extends CommonDao<TermTaxonomy> {
}
