package belog.dao.impl;


import belog.dao.LinksDao;
import belog.dao.common.impl.CommonDaoImpl;
import belog.pojo.po.Links;
import org.springframework.stereotype.Repository;

/**
 * @author Beldon
 */
@Repository("LinksDao")
public class LinksDaoImpl extends CommonDaoImpl<Links> implements LinksDao {

}
