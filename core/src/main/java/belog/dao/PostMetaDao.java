package belog.dao;

import belog.dao.common.CommonDao;
import belog.pojo.po.PostMeta;

/**
 * @author Beldon
 */
public interface PostMetaDao extends CommonDao<PostMeta> {
}
